import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LeaseService} from '../../services/api/lease.service';
import {switchMap, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {mapToData} from '../../services/transformers/response.transformer';

@Component({
    selector: 'app-delivery',
    templateUrl: './delivery.component.html',
    styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {
    @ViewChild('PopUp') popup: ElementRef;

    receiptForm: FormGroup;
    transaction: any;

    constructor(
        private fb: FormBuilder,
        private leaseService: LeaseService,
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.activatedRoute.params.pipe(
            switchMap(params => {
                this.receiptForm = this.fb.group({
                    transaction_id: [params.id],
                    receipt_no: ['', Validators.required]
                });

                return this.getDelivery(params.id);
            })
        ).subscribe();
    }

    submitDelivery() {
        this.leaseService.leaseDelivery(this.receiptForm.getRawValue())
            .pipe(
                switchMap(result => {
                    this.Appear();
                    return this.getDelivery(this.receiptForm.getRawValue()['transaction_id']);
                })
            ).subscribe();
    }

    getDelivery(id: number) {
        return this.leaseService.getById(id).pipe(
            mapToData(),
            tap(transaction => {
                this.transaction = transaction;
            }),
        );
    }

    Appear() {
        this.popup.nativeElement.classList.toggle('Hidden');
    }


}
