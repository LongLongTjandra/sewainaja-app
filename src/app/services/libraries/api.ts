// the wrapper of API to make the use of API easier

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class Api {

    private apiHeader: any;
    private apiParams: any;

    constructor(private http: HttpClient) {
    }

    bindHeader() {
        this.apiHeader = {};

        if (localStorage.getItem('token')) {
            this.apiHeader['Authorization'] = `Bearer ${localStorage.getItem('token')}`;
        }
    }

    bindParams() {
        this.apiParams = {};
    }

    bindOptions() {
        return {
            headers: this.apiHeader,
            params: this.apiParams
        };
    }

    private getUrl(path) {
        return `${environment.API_URL}/${path}`;
    }

    get(path, params = {}) {
        this.bindHeader();
        this.bindParams();

        this.apiParams = {
            ...params
        };

        const options = this.bindOptions();

        return this.http.get(this.getUrl(path), options);
    }

    post(path, body, upload?: any, file?: any) {
        this.bindHeader();
        this.bindParams();

        const options = this.bindOptions();

        const formData = new FormData();
        for (const key in body) {
            if (key in body) {
                formData.append(key, body[key]);
            }
        }

        if (upload) {
            formData.append('image', file);
        }

        return this.http.post(this.getUrl(path), formData, options);
    }

    destroy(path) {
        this.bindHeader();
        this.bindParams();

        const options = this.bindOptions();

        return this.http.delete(this.getUrl(path), options);
    }

    postJson(path, body) {
        this.bindHeader();
        this.bindParams();

        const options = this.bindOptions();

        return this.http.post(this.getUrl(path), body, options);
    }
}
