import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../services/api/auth.service';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {mapToData} from '../../services/transformers/response.transformer';

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

    public loggedIn: boolean;
    public username: string;
    public isProvider: boolean;
    private mouseClick: boolean;

    @ViewChild('Search')search: ElementRef;

    constructor(private authService: AuthService, private router: Router) {
        this.loggedIn = false;
        this.mouseClick = true;
    }

    logout() {
        this.authService.logout().pipe(
            tap(() => {
                localStorage.clear();

                this.router.navigate(['/auth/login']);
            })
        ).subscribe();
    }

    ngOnInit() {
        this.authService.me()
            .pipe(
                mapToData(),
                tap((res: any) => {
                    this.loggedIn = true;
                    this.username = res.name.split(' ')[0];
                    this.isProvider = res.role === 'provider';

                }, err => {
                    this.loggedIn = false;
                })
            ).subscribe();
    }

    SearchPop(){
            if (this.mouseClick === true){
                    this.search.nativeElement.classList.toggle('Length');
                    // this.mouseClick = false;
            }
    }

    searchSmth(key){
            console.log('kamu pencet' + key);
            this.mouseClick = true;
    }
}
