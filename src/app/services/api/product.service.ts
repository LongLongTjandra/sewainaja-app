// the API for displaying product info

import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()

export class ProductService {

    constructor(private api: Api) {
    }

    //to display data to the view

    public getProductDetail(productId: number): Observable<any> {
        return this.api.get(`products/${productId}`);
    }

    //return the most bought item to be displayed

    public getHomeProduct(): Observable<any> {
            return this.api.get('product-mostbought');
    }

    //return a new item
    public getHomeNewProduct(): Observable<any> {
            return this.api.get('product-NewlyCreated');
    }
}
