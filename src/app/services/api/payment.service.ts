// the API for payment

import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';

@Injectable()
export class PaymentService {

    constructor(private api: Api) {}


    public pay(body: any) {
        return this.api.postJson('lease/pay', body);
    }
}
