import {HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {UnaryFunction} from 'rxjs/internal/types';
import {Observable} from 'rxjs/internal/Observable';
import {pipe} from 'rxjs/internal-compatibility';

export function mapToData<T extends HttpResponse<any>>(): UnaryFunction<Observable<any>, Observable<any>> {
    return pipe(
        map((res: any) => {
            if (res.data) {
                return res.data;
            }

            throw new Error('There are no body to be transformed');
        }),
    );
}
