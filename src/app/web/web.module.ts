import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';
import {ProductDetailsComponent} from './product-details/product-details.component';
import {ProductListComponent} from './product-list/product-list.component';
import {FooterComponent} from './footer/footer.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {WebRoutingModule} from './web.routes';
import {WebTemplateComponent} from './web.template';
import {TitleComponent} from './title/title.component';
import {CartComponent} from './cart/cart.component';
import {OrderComponent} from './order/order.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DeliveryComponent} from './delivery/delivery.component';
import {MyProductsComponent} from './my-products/my-products.component';
import {MyProductsCreateFormComponent} from './my-products/forms/my-products-create.form';

const COMPONENTS: any = [
    WebTemplateComponent,
    HomeComponent,
    ProductDetailsComponent,
    ProductListComponent,
    FooterComponent,
    NavBarComponent,
    MyProductsComponent,
    MyProductsCreateFormComponent,
    DeliveryComponent
];

@NgModule({
    imports: [
        CommonModule,
        WebRoutingModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [...COMPONENTS],
    declarations: [...COMPONENTS, TitleComponent, CartComponent, OrderComponent]
})
export class WebModule {}

