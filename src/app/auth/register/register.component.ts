import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/api/auth.service';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {mapToData} from '../../services/transformers/response.transformer';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public registerForm: FormGroup;

    constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {}

    register() {
        this.authService.register(this.registerForm.getRawValue())
            .pipe(
                mapToData(),
                tap((response: any) => {
                    localStorage.setItem('token', response.token);

                    this.router.navigate(['/']);
                }),
            ).subscribe();
    }

    ngOnInit(): void {
        this.registerForm = this.fb.group({
            name: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', Validators.required],
            confirm_password: ['', Validators.required],
        });
    }
}

