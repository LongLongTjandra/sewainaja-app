import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {ServiceModule} from './services/service.module';
import {NgxStripeModule} from 'ngx-stripe';
import { Page404Component } from './page404/page404.component';

@NgModule({
    declarations: [AppComponent, Page404Component],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ServiceModule.forRoot(),
        NgxStripeModule.forRoot('pk_test_AxC0gnjg78FnSHGVI54GhXZm00sHjGixvB')
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
